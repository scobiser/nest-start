/* tslint:disable */
/* eslint-disable */
import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationParams } from './api-configuration';

import { PeopleService } from './services/people.service';
import { RolesGuardService } from './services/roles-guard.service';
import { PipeService } from './services/pipe.service';
import { InterceptorService } from './services/interceptor.service';
import { FilterService } from './services/filter.service';
import { UserService } from './services/user.service';
import { ArticleService } from './services/article.service';
import { RoleService } from './services/role.service';
import { AuthService } from './services/auth.service';
import { FileUploadService } from './services/file-upload.service';
import { Service } from './services/service';
import { HashService } from './services/hash.service';
import { ApiService } from './services/api.service';
import { CrudService } from './services/crud.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    PeopleService,
    RolesGuardService,
    PipeService,
    InterceptorService,
    FilterService,
    UserService,
    ArticleService,
    RoleService,
    AuthService,
    FileUploadService,
    Service,
    HashService,
    ApiService,
    CrudService,
    ApiConfiguration
  ],
})
export class ApiModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor( 
    @Optional() @SkipSelf() parentModule: ApiModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
      'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
