/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Compare } from '../models/compare';
import { Hash } from '../models/hash';

@Injectable({
  providedIn: 'root',
})
export class HashService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation hashControllerHash
   */
  static readonly HashControllerHashPath = '/hash/hash';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `hashControllerHash()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  hashControllerHash$Response(params: {
    context?: HttpContext
    body: Hash
  }
): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, HashService.HashControllerHashPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `hashControllerHash$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  hashControllerHash(params: {
    context?: HttpContext
    body: Hash
  }
): Observable<{
}> {

    return this.hashControllerHash$Response(params).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation hashControllerCompare
   */
  static readonly HashControllerComparePath = '/hash/compare';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `hashControllerCompare()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  hashControllerCompare$Response(params: {
    context?: HttpContext
    body: Compare
  }
): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, HashService.HashControllerComparePath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `hashControllerCompare$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  hashControllerCompare(params: {
    context?: HttpContext
    body: Compare
  }
): Observable<{
}> {

    return this.hashControllerCompare$Response(params).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

}
