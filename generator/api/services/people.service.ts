/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { People } from '../models/people';

@Injectable({
  providedIn: 'root',
})
export class PeopleService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation peopleControllerGetUsers
   */
  static readonly PeopleControllerGetUsersPath = '/people';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerGetUsers()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUsers$Response(params?: {
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerGetUsersPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerGetUsers$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUsers(params?: {
    context?: HttpContext
  }
): Observable<void> {

    return this.peopleControllerGetUsers$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation peopleControllerRegister
   */
  static readonly PeopleControllerRegisterPath = '/people';

  /**
   * 用户注册.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerRegister()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  peopleControllerRegister$Response(params: {
    context?: HttpContext
    body: People
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerRegisterPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * 用户注册.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerRegister$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  peopleControllerRegister(params: {
    context?: HttpContext
    body: People
  }
): Observable<void> {

    return this.peopleControllerRegister$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation peopleControllerGetUserByQuery
   */
  static readonly PeopleControllerGetUserByQueryPath = '/people/query';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerGetUserByQuery()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserByQuery$Response(params: {
    age: any;
    name: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerGetUserByQueryPath, 'get');
    if (params) {
      rb.query('age', params.age, {});
      rb.query('name', params.name, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerGetUserByQuery$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserByQuery(params: {
    age: any;
    name: any;
    context?: HttpContext
  }
): Observable<void> {

    return this.peopleControllerGetUserByQuery$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation peopleControllerGetUserByName
   */
  static readonly PeopleControllerGetUserByNamePath = '/people/{name}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerGetUserByName()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserByName$Response(params: {
    name: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerGetUserByNamePath, 'get');
    if (params) {
      rb.path('name', params.name, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerGetUserByName$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserByName(params: {
    name: any;
    context?: HttpContext
  }
): Observable<void> {

    return this.peopleControllerGetUserByName$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation peopleControllerGetUserById
   */
  static readonly PeopleControllerGetUserByIdPath = '/people/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerGetUserById()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserById$Response(params: {
    id: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerGetUserByIdPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerGetUserById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerGetUserById(params: {
    id: any;
    context?: HttpContext
  }
): Observable<void> {

    return this.peopleControllerGetUserById$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation peopleControllerDeleteUser
   */
  static readonly PeopleControllerDeleteUserPath = '/people/{id}';

  /**
   * 删除用户.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `peopleControllerDeleteUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerDeleteUser$Response(params: {
    id: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PeopleService.PeopleControllerDeleteUserPath, 'delete');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * 删除用户.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `peopleControllerDeleteUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  peopleControllerDeleteUser(params: {
    id: any;
    context?: HttpContext
  }
): Observable<void> {

    return this.peopleControllerDeleteUser$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

}
