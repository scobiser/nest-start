/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { BufferRes } from '../models/buffer-res';
import { EncryptText } from '../models/encrypt-text';

@Injectable({
  providedIn: 'root',
})
export class Service extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation cryptControllerPlain
   */
  static readonly CryptControllerPlainPath = '/crypt/text-plain-content';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cryptControllerPlain()` instead.
   *
   * This method sends `text/plain` and handles request body of type `text/plain`.
   */
  cryptControllerPlain$Response(params: {
    context?: HttpContext
    body: string
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, Service.CryptControllerPlainPath, 'post');
    if (params) {
      rb.body(params.body, 'text/plain');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cryptControllerPlain$Response()` instead.
   *
   * This method sends `text/plain` and handles request body of type `text/plain`.
   */
  cryptControllerPlain(params: {
    context?: HttpContext
    body: string
  }
): Observable<string> {

    return this.cryptControllerPlain$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * Path part for operation cryptControllerEncrypt
   */
  static readonly CryptControllerEncryptPath = '/crypt/encrypt';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cryptControllerEncrypt()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cryptControllerEncrypt$Response(params: {
    context?: HttpContext
    body: EncryptText
  }
): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, Service.CryptControllerEncryptPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cryptControllerEncrypt$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cryptControllerEncrypt(params: {
    context?: HttpContext
    body: EncryptText
  }
): Observable<{
}> {

    return this.cryptControllerEncrypt$Response(params).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation cryptControllerDecrypt
   */
  static readonly CryptControllerDecryptPath = '/crypt/decrypt';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cryptControllerDecrypt()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cryptControllerDecrypt$Response(params: {
    context?: HttpContext
    body: BufferRes
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, Service.CryptControllerDecryptPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cryptControllerDecrypt$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cryptControllerDecrypt(params: {
    context?: HttpContext
    body: BufferRes
  }
): Observable<string> {

    return this.cryptControllerDecrypt$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

}
