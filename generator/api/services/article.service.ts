/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Article } from '../models/article';

@Injectable({
  providedIn: 'root',
})
export class ArticleService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation articleControllerRegister
   */
  static readonly ArticleControllerRegisterPath = '/blogArticle/allArticle';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `articleControllerRegister()` instead.
   *
   * This method doesn't expect any request body.
   */
  articleControllerRegister$Response(params: {
    limit: any;
    index: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<Array<Article>>> {

    const rb = new RequestBuilder(this.rootUrl, ArticleService.ArticleControllerRegisterPath, 'get');
    if (params) {
      rb.path('limit', params.limit, {});
      rb.path('index', params.index, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Article>>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `articleControllerRegister$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  articleControllerRegister(params: {
    limit: any;
    index: any;
    context?: HttpContext
  }
): Observable<Array<Article>> {

    return this.articleControllerRegister$Response(params).pipe(
      map((r: StrictHttpResponse<Array<Article>>) => r.body as Array<Article>)
    );
  }

  /**
   * Path part for operation articleControllerArticleCount
   */
  static readonly ArticleControllerArticleCountPath = '/blogArticle/articleCount';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `articleControllerArticleCount()` instead.
   *
   * This method doesn't expect any request body.
   */
  articleControllerArticleCount$Response(params?: {
    context?: HttpContext
  }
): Observable<StrictHttpResponse<number>> {

    const rb = new RequestBuilder(this.rootUrl, ArticleService.ArticleControllerArticleCountPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: parseFloat(String((r as HttpResponse<any>).body)) }) as StrictHttpResponse<number>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `articleControllerArticleCount$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  articleControllerArticleCount(params?: {
    context?: HttpContext
  }
): Observable<number> {

    return this.articleControllerArticleCount$Response(params).pipe(
      map((r: StrictHttpResponse<number>) => r.body as number)
    );
  }

}
