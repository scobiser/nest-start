/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Animal } from '../models/animal';
import { Pet } from '../models/pet';

@Injectable({
  providedIn: 'root',
})
export class PipeService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation pipeControllerGetPipe
   */
  static readonly PipeControllerGetPipePath = '/pipe';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetPipe()` instead.
   *
   * This method doesn't expect any request body.
   */
  pipeControllerGetPipe$Response(params?: {
    name?: string;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetPipePath, 'get');
    if (params) {
      rb.query('name', params.name, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetPipe$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  pipeControllerGetPipe(params?: {
    name?: string;
    context?: HttpContext
  }
): Observable<void> {

    return this.pipeControllerGetPipe$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation pipeControllerGetPipeById
   */
  static readonly PipeControllerGetPipeByIdPath = '/pipe/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetPipeById()` instead.
   *
   * This method doesn't expect any request body.
   */
  pipeControllerGetPipeById$Response(params: {
    id: number;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetPipeByIdPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetPipeById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  pipeControllerGetPipeById(params: {
    id: number;
    context?: HttpContext
  }
): Observable<void> {

    return this.pipeControllerGetPipeById$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation pipeControllerGetPipeByAnimalJoi
   */
  static readonly PipeControllerGetPipeByAnimalJoiPath = '/pipe/animalJoi';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetPipeByAnimalJoi()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalJoi$Response(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetPipeByAnimalJoiPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetPipeByAnimalJoi$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalJoi(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<void> {

    return this.pipeControllerGetPipeByAnimalJoi$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation pipeControllerGetPipeByAnimalClassBody
   */
  static readonly PipeControllerGetPipeByAnimalClassBodyPath = '/pipe/animalClassBody';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetPipeByAnimalClassBody()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalClassBody$Response(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetPipeByAnimalClassBodyPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetPipeByAnimalClassBody$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalClassBody(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<void> {

    return this.pipeControllerGetPipeByAnimalClassBody$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation pipeControllerGetPipeByAnimalClassUsePipe
   */
  static readonly PipeControllerGetPipeByAnimalClassUsePipePath = '/pipe/animalClassUsePipe';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetPipeByAnimalClassUsePipe()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalClassUsePipe$Response(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetPipeByAnimalClassUsePipePath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetPipeByAnimalClassUsePipe$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetPipeByAnimalClassUsePipe(params: {
    context?: HttpContext
    body: Animal
  }
): Observable<void> {

    return this.pipeControllerGetPipeByAnimalClassUsePipe$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation pipeControllerGetNoPipe
   */
  static readonly PipeControllerGetNoPipePath = '/pipe/nopipe';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pipeControllerGetNoPipe()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetNoPipe$Response(params: {
    context?: HttpContext
    body: Pet
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, PipeService.PipeControllerGetNoPipePath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pipeControllerGetNoPipe$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pipeControllerGetNoPipe(params: {
    context?: HttpContext
    body: Pet
  }
): Observable<void> {

    return this.pipeControllerGetNoPipe$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

}
