/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class InterceptorService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation intercetptorControllerIntercept
   */
  static readonly IntercetptorControllerInterceptPath = '/intercetptor/intercept';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `intercetptorControllerIntercept()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerIntercept$Response(params?: {
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, InterceptorService.IntercetptorControllerInterceptPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `intercetptorControllerIntercept$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerIntercept(params?: {
    context?: HttpContext
  }
): Observable<void> {

    return this.intercetptorControllerIntercept$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation intercetptorControllerTransform
   */
  static readonly IntercetptorControllerTransformPath = '/intercetptor/transform';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `intercetptorControllerTransform()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerTransform$Response(params?: {
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, InterceptorService.IntercetptorControllerTransformPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `intercetptorControllerTransform$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerTransform(params?: {
    context?: HttpContext
  }
): Observable<void> {

    return this.intercetptorControllerTransform$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation intercetptorControllerErrors
   */
  static readonly IntercetptorControllerErrorsPath = '/intercetptor/errors';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `intercetptorControllerErrors()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerErrors$Response(params?: {
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, InterceptorService.IntercetptorControllerErrorsPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `intercetptorControllerErrors$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerErrors(params?: {
    context?: HttpContext
  }
): Observable<void> {

    return this.intercetptorControllerErrors$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation intercetptorControllerCache
   */
  static readonly IntercetptorControllerCachePath = '/intercetptor/cache';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `intercetptorControllerCache()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerCache$Response(params: {
    data: any;
    context?: HttpContext
  }
): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, InterceptorService.IntercetptorControllerCachePath, 'get');
    if (params) {
      rb.query('data', params.data, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: params?.context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `intercetptorControllerCache$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  intercetptorControllerCache(params: {
    data: any;
    context?: HttpContext
  }
): Observable<void> {

    return this.intercetptorControllerCache$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

}
