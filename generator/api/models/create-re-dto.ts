/* tslint:disable */
/* eslint-disable */
export interface CreateReDto {

  /**
   * 年龄
   */
  age: number;

  /**
   * 名字
   */
  username: string;
}
