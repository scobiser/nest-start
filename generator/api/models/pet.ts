/* tslint:disable */
/* eslint-disable */
export interface Pet {

  /**
   * 年龄
   */
  age: number;

  /**
   * 名字
   */
  name: string;
}
