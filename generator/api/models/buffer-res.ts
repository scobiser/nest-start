/* tslint:disable */
/* eslint-disable */
export interface BufferRes {
  data: Array<string>;
  type: string;
}
