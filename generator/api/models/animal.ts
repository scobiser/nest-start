/* tslint:disable */
/* eslint-disable */
export interface Animal {

  /**
   * 年龄
   */
  age: number;

  /**
   * 名字
   */
  name: string;
}
