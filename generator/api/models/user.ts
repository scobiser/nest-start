/* tslint:disable */
/* eslint-disable */
export interface User {
  address: string;
  avatar: string;
  del: {
};
  email: string;
  id: number;
  last_login_time: null | string;
  nick_name: string;
  password: string;
  phone: string;
  role_id: string;
  token: string;
  user_name: string;
}
