/* tslint:disable */
/* eslint-disable */
export interface LoginParam {

  /**
   * 密码
   */
  password: string;

  /**
   * 用户名
   */
  username: string;
}
