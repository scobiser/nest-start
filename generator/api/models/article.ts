/* tslint:disable */
/* eslint-disable */
export interface Article {
  article_name: string;
  article_text: string;
  author_id: number;
  create_time: string;
  deleted: number;
  id: number;
  like_num: number;
  poster: string;
  private: number;
  read_num: number;
  summary: string;
  update_time: string;
}
