import * as rawbody from 'raw-body';

import { createParamDecorator, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
export const PlainBody = createParamDecorator(
    async (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest();
        if (request.readable) {
            return (await rawbody(request)).toString().trim();
        }
        throw new HttpException('Body aint text/plain', HttpStatus.INTERNAL_SERVER_ERROR);
    },
);

