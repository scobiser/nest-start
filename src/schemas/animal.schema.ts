const Joi = require('joi');

export default Joi.object({
    name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    age: Joi.number()
        .integer()
        .min(1900)
        .max(2013)
        .required()
});