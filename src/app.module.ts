import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { DateMiddleware } from './middleware/date.middleware';
import { MongooseModule } from '@nestjs/mongoose';
import { funMiddleware } from './middleware/funMiddleware';
import { RoleGuardsModule } from './modules/role-guards/role-guards.module';
import { PipeModule } from './modules/pipe/pipe.module';
import { HttpInterceptorsModule } from './modules/http-interceptors/http-interceptors.module';
import { FilterModule } from './modules/filter/filter.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DataBase, RegDataBase } from './config/configuration';
import { getRUNNING_ENV } from './config/config.util';
import { getYamlConfig } from './config/yaml.config';
import { YamlData } from './config/yaml.namespace';
import { PeopleModule } from './modules/people/people.module';
import { BlogModule } from './modules/blog/blog.module';
import { AuthModule } from './modules/auth/auth.module';
import { FileUploadModule } from './modules/file-upload/file-upload.module';
import { CryptModule } from './modules/crypt/crypt.module';
import { HashModule } from './modules/hash/hash.module';
import { AsyncModule } from './modules/async/async.module';
import { getJsonConfig } from './config/json.config';
import { ConfigData } from './config/json.config.namespace';
import { ResModule } from './modules/res/res.module';
import { DocModule } from './modules/doc/doc.module';
import { DefaultDB } from './db/datasource/default.datasource';
import { BlogDevDB } from './db/datasource/blog.datasource';


console.log('process :>> ', getRUNNING_ENV());
const envFilePath = '.env.' + getRUNNING_ENV();
console.log('envFilePath :>> ', envFilePath);



@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath,
      isGlobal: true,
      load: [DataBase, RegDataBase, getYamlConfig, YamlData,getJsonConfig,ConfigData]
    }),
    DefaultDB,
    // BlogDevDB,
    DocModule,
    PeopleModule,
    RoleGuardsModule,
    PipeModule,
    HttpInterceptorsModule,
    FilterModule,
    // BlogModule,
    AuthModule,
    FileUploadModule,
    CryptModule,
    HashModule,
    AsyncModule,
    ResModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  constructor(private configService: ConfigService) {
    console.log('envFilePath :>> ', envFilePath);
    console.log('DataBase :>> ', DataBase());
    console.log('getYamlConfig :>> ', getYamlConfig());
    console.log('RegDataBase :>> ', RegDataBase());
    console.log('getJsonConfig :>> ', getJsonConfig());
    console.log('ConfigData :>> ', ConfigData());
    console.log('process.env.DB_HOST :>> ', process.env.DB_HOST);
    console.log('configService DATABASE_USER :>> ', this.configService.get('DB_USERNAME'));
    console.log('configService port :>> ', this.configService.get('port'));
    console.log('configService database.host :>> ', this.configService.get('database.host'));
    console.log('configService regdatabase.port :>> ', this.configService.get('regdatabase.port'));
    console.log('configService yaml env :>> ', this.configService.get('env'));
    console.log('configService yaml env :>> ', this.configService.get('yaml.env'));
  }
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware, DateMiddleware, funMiddleware)
      .forRoutes('');
  }
}
