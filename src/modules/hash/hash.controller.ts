import { Body, Controller, Post } from '@nestjs/common';
import { ApiBody, ApiProperty, ApiTags } from '@nestjs/swagger';
import { HashService } from './hash.service';
class Hash {
    @ApiProperty()
    plainText: string
}
class Compare {
    @ApiProperty()
    plainText: string
    @ApiProperty()
    hash: string
}
@Controller('hash')
@ApiTags("Hash散列")
export class HashController {
    constructor(private hashService: HashService) {

    }
    @Post("hash")
    @ApiBody({ type: Hash })
    hash(@Body('plainText') plainText) {
        return this.hashService.hash(plainText)
    }

    @Post("compare")
    @ApiBody({ type: Compare })
    compare(@Body('plainText') plainText: string,@Body('hash') hash: string,) {
        return this.hashService.compare(plainText, hash)
    }
}
