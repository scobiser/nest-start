import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { HASH_SALT_AROUND } from 'src/constants/hash.constants';

@Injectable()
export class HashService {
    async hash(planinText: string) {
        return await bcrypt.hash(planinText, HASH_SALT_AROUND);
    }

    async compare(plainText: string, hash: string) {
        return await bcrypt.compare(plainText, hash);
    }
}
