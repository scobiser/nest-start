import { Controller, ForbiddenException, Get, HttpCode, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/filter/http-exception.filter';

@Controller('filter')
@ApiTags("filter")
export class FilterController {
    constructor() {
       
    }
    @Get('exception')
    @UseFilters(HttpExceptionFilter)
    @HttpCode(200)
    filterException() {
 
        throw new ForbiddenException();
        // return new ForbiddenException();
    }
}
