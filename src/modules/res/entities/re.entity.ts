import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 're' })
export class Re {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  username: string;
  @Column()
  age: number;
}