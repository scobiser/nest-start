import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateReDto {
    @ApiProperty({ description: '名字' })
    @IsNotEmpty({ message: '名字不能为空' })
    username: string;
    @ApiProperty({ description: '年龄' })
    @IsNotEmpty({ message: '年龄不能为空' })
    age: number;
}
