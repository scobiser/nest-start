import { Module } from '@nestjs/common';
import { ResService } from './res.service';
import { ResController } from './res.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Re } from './entities/re.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Re])],
  controllers: [ResController],
  providers: [ResService]
})
export class ResModule {}
