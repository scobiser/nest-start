import { Injectable } from '@nestjs/common';
import { People } from 'src/entity/People.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReDto } from './dto/create-re.dto';
import { UpdateReDto } from './dto/update-re.dto';
import { Re } from './entities/re.entity';

@Injectable()
export class ResService {
  constructor(
    @InjectRepository(Re)
    private reRepository: Repository<Re>) { }
  create(createReDto: CreateReDto) {
    return this.reRepository.save(createReDto)
  }

  findAll() {
    return this.reRepository.find()
  }

  findOne(id: number) {
    return this.reRepository.findOne({ where: { id } })
  }

  update(id: number, updateReDto: UpdateReDto) {
    this.reRepository.update(id,updateReDto)
  }

  remove(id: number) {
    this.reRepository.delete(id)
  }
}
