import { Body, Controller, Get, Param, ParseIntPipe, Post, Query, UsePipes } from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import AnimalSchema from 'src/schemas/animal.schema';
import { ValidPipe } from 'src/pipes/valid.pipe';
import { JoiValidationPipe } from 'src/pipes/joi-validation.pipe';
import { Animal } from 'src/dto/Animal.dto';
import { ClassValidPipe } from 'src/pipes/class-valid.pipe';
import { Pet } from 'src/dto/Pet.dto';

@Controller('pipe')
@ApiTags("pipe")
export class PipeController {

    @Get('')
    @ApiQuery({ name: 'name', required: false })
    getPipe(@Query('name', ValidPipe) name: string) {
        return name
    }
    
    @Get(':id')
    @ApiParam({ name: 'id', required: true })
    getPipeById(@Param('id', ParseIntPipe) id: number) {
        return id
    }

    @Post('animalJoi')
    @UsePipes(new JoiValidationPipe(AnimalSchema))
    getPipeByAnimalJoi(@Body() animal: Animal) {
        return animal
    }

    @Post('animalClassBody')
    getPipeByAnimalClassBody(@Body(ClassValidPipe) animal: Animal) {
        return animal
    }

    @Post('animalClassUsePipe')
    @UsePipes(ClassValidPipe)
    getPipeByAnimalClassUsePipe(@Body() animal: Animal) {
        return animal
    }

    @Post('nopipe')
    @UsePipes(ClassValidPipe)
    getNoPipe(@Body() pet: Pet) {
        return pet
    }
}
