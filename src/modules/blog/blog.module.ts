import { Module } from '@nestjs/common';
import { ArticleModule } from './article/article.module';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { UserService } from './user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user/user.entity';

@Module({
  imports: [UserModule, ArticleModule, RoleModule,TypeOrmModule.forFeature([User])],
  exports: [UserModule, ArticleModule, RoleModule],
  providers: [UserService],
})
export class BlogModule {}
