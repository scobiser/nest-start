
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Article } from './article.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article,'blog')
    private ArticleRepository: Repository<Article>,
  ) {
  }

  findAll(index: number, limit: number) {
    // return await this.ArticleRepository.find();
    return this.ArticleRepository.find();
  }

  articleCount(): Promise<number> {
    // { skip: 10, take: 20 }
    return this.ArticleRepository.count();
  }
}
