import { Controller, Get, HttpCode, Param, Post } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { ArticleService } from './article.service';

@Controller('blogArticle')
@ApiTags('article')
export class ArticleController {
  constructor(private articleService: ArticleService) {}
  @Get('/allArticle')
  @HttpCode(200)
  @ApiParam({name:'index'})
  @ApiParam({name:'limit'})
  public  register(@Param() index: number, @Param() limit: number) {
    const article = this.articleService.findAll(index, limit);
    return article;
  }

  @Get('/articleCount')
  @HttpCode(200)
  public  articleCount() {
    return this.articleService.articleCount();
  }
}
