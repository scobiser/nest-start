import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/dto/CreateUser.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('blogUser')
@ApiTags('User')
export class UserController {
  constructor(private userService: UserService) {}
  @Post('')
  @HttpCode(200)
  public async register(@Body() body: CreateUserDto) {
    console.log(body);
    return await this.userService.create(body);
  }

  @Get('')
  @HttpCode(200)
  public allUsers() {
    return this.userService.findAll();
  }

  @Get(':id')
  @HttpCode(200)
  public async findOne(@Param('id') id) {
    return await this.userService.findOne(id);
  }

  @Put(':id')
  @HttpCode(200)
  public updateUser(@Param('id') id,@Body() body:User){
    return this.userService.update(id,body)
  }

  @Delete(':id')
  public delete(@Param('id') id){
    return this.userService.delete(id)
  }
}
