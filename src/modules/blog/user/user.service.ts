import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/dto/CreateUser.dto';
import { FindOneOptions, Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {
  }

  async create(user: CreateUserDto) {
    await this.userRepository.save(Object.assign(new User(), user));
    return 'ok';
  }

  async findAll() {
    return await this.userRepository.find();
  }

  async findOne(id: number) {
    const option: FindOneOptions<User> = {
      where: {
        id,
      },
    };
    return await this.userRepository.findOne(option);
  }

  async update(id: number, user: User) {
    return await this.userRepository.update(id, user);
  }

  async delete(id: number) {
    return await this.userRepository.update(id, { del: 1 });
  }
}
