import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';

@Module({
  // forFeature() 方法定义在当前范围中注册哪些存储库
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UserService],
  controllers: [UserController],
  // 此处导出之后 其他导入了user模块的地方都可以用
  // exports: [TypeOrmModule],
})
export class UserModule {}
