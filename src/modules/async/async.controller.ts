import { Controller, Get, Query } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';

@Controller('async')
export class AsyncController {
    @Get('')
    @ApiQuery({ name: 'async' })
    async asuncfun(@Query('async') query: string): Promise<string[]> {
        let res:string[] = [];
        res.push(query)
        return res
    }
}
