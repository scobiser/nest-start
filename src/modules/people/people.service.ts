import { Injectable } from '@nestjs/common';
import { People } from 'src/entity/People.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class PeopleService {

  constructor(@InjectRepository(People)
  private peopleRepository: Repository<People>) {

  }
  getPeoples() {
    return this.peopleRepository.find({
      relations: {
        animals: true,
      }
    });
  }
  getByNameAge(username: string, age: number) {
    return this.peopleRepository.findBy({ username, age });
  }

  findOneById(id: number) {
    return this.peopleRepository.findOneBy({ id });
  }

  getByName(username: string) {
    return this.peopleRepository.findOne({
      where: {
        username
      },
      relations: {
        animals: true,
        roles:true
      }
    });
  }

  createPeople(people: People) {
    return this.peopleRepository.save(people);
  }

  deletePeople(id: number) {
    this.peopleRepository.delete({ id });
  }
}
