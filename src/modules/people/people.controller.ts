import {
  Controller,
  Get,
  Delete,
  HttpCode,
  Param,
  Query,
  Req,
  Post,
  Body,
  UsePipes,
  UseFilters,
  UseGuards,
  SetMetadata,
} from '@nestjs/common';
import { PeopleService } from './people.service';
import { Request } from 'express';
import {
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { UserFilter } from 'src/filter/user.filter';
import { IsNotEmptyPipe } from 'src/pipes/is-not-empty.pipe';
import { People } from 'src/entity/People.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
@Controller('people')
@ApiTags('people')
export class PeopleController {
  constructor(private people: PeopleService) {}
  @Get('')
  @HttpCode(200)
  getUsers() {
    return this.people.getPeoples();
  }

  @Get('query')
  @ApiQuery({ name: 'name' })
  @ApiQuery({ name: 'age' })
  @HttpCode(200)
  getUserByQuery(@Query('name') name, @Query('age') age) {
    return this.people.getByNameAge(name, age);
  }
  @Get(':name')
  @ApiParam({ name: 'name' })
  @HttpCode(200)
  getUserByName(@Param('name') name) {
    console.log('name :>> ', name);
    return this.people.getByName(name);
  }

  @Get(':id')
  @ApiParam({ name: 'id' })
  @HttpCode(200)
  getUserById(@Param('id') id) {
    console.log('id :>> ', id);
    return this.people.findOneById(id);
  }

  @Post()
  @HttpCode(200)
  @UseFilters(new UserFilter())
  @ApiOperation({ summary: '用户注册' })
  register(@Body(IsNotEmptyPipe) people: People) {
    console.log('user :>> ', people);
    return this.people.createPeople(people);
  }
  @Delete(':id')
  @HttpCode(200)
  @ApiOperation({ summary: '删除用户' })
  @ApiParam({ name: 'id' })
  deleteUser(@Param('id') id) {
    console.log('id :>> ', id);
    return this.people.deletePeople(id);
  }
}
