import { Body, Controller, Post, Query, Req, Res } from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiHeader, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { CryptService } from './crypt.service';
import { Request } from 'express';
import * as rawbody from 'raw-body';
import { PlainBody } from 'src/decorators/PlainBody.decorator';
class EncryptText {
    @ApiProperty({ type: 'string' })
    text: string
}
export class BufferRes {
    @ApiProperty()
    type: string;
    @ApiProperty()
    data: number[];
}
@Controller('crypt')
@ApiTags("加密")
export class CryptController {
    constructor(private cryptService: CryptService) {

    }
    @Post("text-plain-content")
    @ApiConsumes('text/plain')
    @ApiBody({ type: String })
    async plain(@PlainBody() plain: string, @Req() req) {
        console.log('plain :>> ', plain);
        return plain
    }
    @Post("encrypt")
    @ApiBody({ type: EncryptText })
    encrypt(@Body() plainText: EncryptText) {
        console.log('plainText :>> ', plainText);
        return this.cryptService.encrypt(plainText.text);
    }
    @Post("decrypt")
    @ApiBody({ type: BufferRes })
    decrypt(@Body() decryptedText: BufferRes) {
        return this.cryptService.decrypt(decryptedText);
    }
}


