import { Module } from '@nestjs/common';
import { CryptController } from './crypt.controller';
import { CryptService } from './crypt.service';

@Module({
  controllers: [CryptController],
  providers: [CryptService]
})
export class CryptModule {}
