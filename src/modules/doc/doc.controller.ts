import { Controller, Get, Redirect } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@Controller('openapi-json')
@ApiTags("openapi.json")
export class DocController {
    @Get('json')
    @Redirect('/api-json', 302)
    getDocs() {

    }
}
