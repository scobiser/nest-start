import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PeopleService } from '../people/people.service';

@Injectable()
export class AuthService {

    constructor(private readonly peopleService: PeopleService, private readonly jwtService: JwtService) { }
    /**
     * @result 就是返回的内容
     * @param username 
     * @param pass 
     */
    async validateUser(username: string, pass: string): Promise<any> {
        console.log('AuthService :>> ', username);
        const people = await this.peopleService.getByName(username);
        if (people) {
            if (people && people.password === pass) {
                const { password, ...result } = people;
                return result;
            }
        }
        return null;
    }

    async login(user: any) {
        const payload = { username: user.username, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    findUserByName(username:string){
        return this.peopleService.getByName(username);
    }
}
