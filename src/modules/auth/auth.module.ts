import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PeopleModule } from '../people/people.module';
import { LocalStrategy } from './local-strategy/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constants/jwt.constants';
import { JwtStrategyService } from './jwt-strategy/jwt-strategy.service';


@Module({
  imports: [PeopleModule, PassportModule, JwtModule.register({
    secret: jwtConstants.secret,
    signOptions: { expiresIn: '7200s' },
  })],
  providers: [AuthService, LocalStrategy, JwtStrategyService],
  controllers: [AuthController]
})
export class AuthModule { }
