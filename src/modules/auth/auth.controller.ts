import { Controller, Post, Body, Request, UseGuards, Get } from '@nestjs/common';
import { ApiBody, ApiTags, ApiProperty } from '@nestjs/swagger';
import { People } from 'src/entity/People.entity';
import { Entity, Column } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { Roles } from 'src/roles/roles.decorator';
import { Role } from 'src/enums/role.enum';
import { RolesGuard } from 'src/guards/roles.guard';
class LoginParam {
    @ApiProperty({ description: '用户名' })
    username: string;
    @ApiProperty({ description: '密码' })
    password: string
}
@Controller('auth')
@ApiTags("Auth")
export class AuthController {
    constructor(private authServe: AuthService) {

    }
    @UseGuards(AuthGuard('local'))
    @Post('login')
    @ApiBody({ type: LoginParam })
    login(@Body() people: People) {
        console.log('auth/login people :>> ', people);
        return people;
    }

    @UseGuards(AuthGuard('local'))
    @Post('loginToken')
    @ApiBody({ type: LoginParam })
    auth(@Request() req) {
        console.log('auth/login req :>> ', req.user);
        return this.authServe.login(req.user);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('profile')
    getProfile(@Request() req) {
        return req.user;
    }

    @Get('role-guards')
    @Roles(Role.Admin)
    @UseGuards(AuthGuard('jwt'),RolesGuard)
    roleGuards(@Request() req) {
        return req.user;
    }
}
