import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from 'src/constants/jwt.constants';
import { AuthService } from '../auth.service';


@Injectable()
export class JwtStrategyService extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.secret,
        });
    }
    /**
     * 到这一步已经过了TOKEN的验证部分
     * @param payload 
     * @returns 
     */
    async validate(payload: any) {
        console.log('payload :>> ', payload);
        const { username } = payload;
        const user = await this.authService.findUserByName(username);
        return { userId: payload.sub, username: payload.username ,roles:user.roles.map(item=>item.role)};
    }
}