import { Module } from '@nestjs/common';
import { IntercetptorController } from './intercetptor.controller';
import { IntercetptorService } from './intercetptor.service';

@Module({
  controllers: [IntercetptorController],
  providers: [IntercetptorService]
})
export class HttpInterceptorsModule {}
