import { Controller, Get, HttpCode, UseInterceptors } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { CacheInterceptor } from 'src/interceptors/cache.interceptor';
import { ErrorsInterceptor } from 'src/interceptors/errors.interceptor';
import { LogInterceptor } from 'src/interceptors/log.interceptor';
import { TranformInterceptor } from 'src/interceptors/tranform.interceptor';
import { IntercetptorService } from './intercetptor.service';

@Controller('intercetptor')

@ApiTags("Interceptor")
export class IntercetptorController {
    constructor(private interceptServe: IntercetptorService) {

    }
    @Get('intercept')
    @UseInterceptors(LogInterceptor)
    @HttpCode(200)
    intercept() {
        return this.interceptServe.getIntercept();
    }
    @Get('transform')
    @UseInterceptors(TranformInterceptor)
    @HttpCode(200)
    transform() {
        return this.interceptServe.getIntercept();
    }
    @Get('errors')
    @UseInterceptors(ErrorsInterceptor)
    @HttpCode(200)
    errors() {
        return this.interceptServe.error();
    }
    @Get('cache')
    @UseInterceptors(CacheInterceptor)
    @ApiQuery({name:"data",required:true})
    @HttpCode(200)
    cache() {
        return this.interceptServe.cache();
    }
}
