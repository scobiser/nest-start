import { Test, TestingModule } from '@nestjs/testing';
import { IntercetptorService } from './intercetptor.service';

describe('IntercetptorService', () => {
  let service: IntercetptorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IntercetptorService],
    }).compile();

    service = module.get<IntercetptorService>(IntercetptorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
