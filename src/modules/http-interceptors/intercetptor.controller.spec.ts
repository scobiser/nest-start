import { Test, TestingModule } from '@nestjs/testing';
import { IntercetptorController } from './intercetptor.controller';

describe('IntercetptorController', () => {
  let controller: IntercetptorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IntercetptorController],
    }).compile();

    controller = module.get<IntercetptorController>(IntercetptorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
