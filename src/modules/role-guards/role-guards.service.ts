import { Injectable } from '@nestjs/common';

@Injectable()
export class RoleGuardsService {
    getAdminRole(){
        return {
            "role":"admin"
        }
    }
}
