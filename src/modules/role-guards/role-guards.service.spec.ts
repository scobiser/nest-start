import { Test, TestingModule } from '@nestjs/testing';
import { RoleGuardsService } from './role-guards.service';

describe('RoleGuardsService', () => {
  let service: RoleGuardsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleGuardsService],
    }).compile();

    service = module.get<RoleGuardsService>(RoleGuardsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
