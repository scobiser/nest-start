import { Test, TestingModule } from '@nestjs/testing';
import { RoleGuardsController } from './role-guards.controller';

describe('RoleGuardsController', () => {
  let controller: RoleGuardsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleGuardsController],
    }).compile();

    controller = module.get<RoleGuardsController>(RoleGuardsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
