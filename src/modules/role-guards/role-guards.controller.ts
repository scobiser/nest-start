import { Body, Controller, Get, HttpCode, Post, UseGuards } from '@nestjs/common';
import { ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from 'src/guards/roles.guard';
import { Roles } from 'src/roles/roles.decorator';
import { RoleGuardsService } from './role-guards.service';
import { Role } from '../../enums/role.enum'
@Controller('role-guards')
@UseGuards(RolesGuard)
@ApiTags("RolesGuard")
export class RoleGuardsController {
    constructor(private roleGuards: RoleGuardsService) {

    }
    @Get('user')
    @Roles(Role.Admin)
    @HttpCode(200)
    getAdmin(@Body() roles) {
        return this.roleGuards.getAdminRole()
    }

}
