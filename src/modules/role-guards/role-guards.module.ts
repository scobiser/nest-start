import { Module } from '@nestjs/common';
import { RoleGuardsController } from './role-guards.controller';
import { RoleGuardsService } from './role-guards.service';

@Module({
  providers: [RoleGuardsService],
  controllers: [RoleGuardsController]
})
export class RoleGuardsModule {}
