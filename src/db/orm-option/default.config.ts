import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { getYamlConfig } from "../../config/yaml.config";
import { Animal } from "../../entity/Animal.entity";
import { Dog } from "../../entity/Dog";
import { People } from "../../entity/People.entity";
import { Role } from "../../entity/role.entity";
import { User } from "../../modules/blog/user/user.entity";
import { Re } from "../../modules/res/entities/re.entity";
const dbConfig = getYamlConfig();
console.log('__dirname :>> ', process.cwd() + 'src/db/migration/*.ts');
export const  DefaultDBOption: TypeOrmModuleOptions = {
  type: 'mysql',
  host: dbConfig.db.mysql.url,
  port: dbConfig.db.mysql.port,
  username: dbConfig.db.mysql.user,
  password: dbConfig.db.mysql.password,
  database: dbConfig.db.mysql.database,
  entities: [People, User, Animal, Re,Dog,Role],
  autoLoadEntities: true,
  synchronize: false
};
