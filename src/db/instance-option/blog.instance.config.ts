
import { TypeOrmModuleOptions } from "@nestjs/typeorm"
import * as path from "path";
import { getYamlConfig } from "../../config/yaml.config";
import { Article } from "../../modules/blog/article/article.entity";
const dbConfig = getYamlConfig();
// console.log('__dirname :>> ', process.cwd());
export const BlogDevDBInstanceOption: TypeOrmModuleOptions = {
    type: 'mysql',
    host: dbConfig.db.mysql2.url,
    port: dbConfig.db.mysql2.port,
    username: dbConfig.db.mysql2.user,
    password: dbConfig.db.mysql2.password,
    database: dbConfig.db.mysql2.database,
    name: 'blog',
    entities: [Article],
    synchronize: false,
    migrations: [path.resolve(process.cwd(), './src/migration/*.ts')],
};