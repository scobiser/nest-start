
import * as path from "path";
import { DataSourceOptions } from "typeorm";
import { getYamlConfig } from "../../config/yaml.config";
import { Re } from "../../modules/res/entities/re.entity";
const dbConfig = getYamlConfig();
console.log('__dirname :>> ', process.cwd(), path.resolve(process.cwd(), './src/migration/*.ts'));
export const DefaultDBInstanceOption: DataSourceOptions = {
  type: 'mysql',
  host: dbConfig.db.mysql.url,
  port: dbConfig.db.mysql.port,
  username: dbConfig.db.mysql.user,
  password: dbConfig.db.mysql.password,
  database: dbConfig.db.mysql.database,
  entities: [Re],
  synchronize: false,
  // migration读取地址
  migrations: [path.resolve(process.cwd(), './src/migrations/*.ts')],
  migrationsTableName: "migrations",
  migrationsRun: true
};
// > nest-starter@0.0.1 typeorm:run-migrations-dev
// > npm run typeorm-dev migration:run -- -d ./src/db/instance/default.instance.ts


// > nest-starter@0.0.1 typeorm-dev
// > cross-env  RUNNING_ENV=development ts-node ./node_modules/typeorm/cli "migration:run" "-d" "./src/db/instance/default.instance.ts"

// __dirname :>>  E:\Gitee\nest-starter E:\Gitee\nest-starter\src\migration\*.ts
// query: SELECT VERSION() AS `version`
// query: SELECT VERSION() AS `version`
// query: SELECT * FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA` = 'nest' AND `TABLE_NAME` = 'migrations'
// query: SELECT * FROM `nest`.`migrations` `migrations` ORDER BY `id` DESC
// No migrations are pending