import { TypeOrmModule } from "@nestjs/typeorm";
import { BlogDevDBOption } from "../orm-option/blog.config";

export const BlogDevDB = TypeOrmModule.forRoot(BlogDevDBOption);