import { TypeOrmModule } from "@nestjs/typeorm";
import { DefaultDBOption } from "../orm-option/default.config";

export const DefaultDB = TypeOrmModule.forRoot(DefaultDBOption);