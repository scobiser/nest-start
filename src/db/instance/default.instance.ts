import { DataSource } from "typeorm";
import { DefaultDBInstanceOption } from "../instance-option/default.instance.config";
const  DefaultDB = new DataSource(DefaultDBInstanceOption);
DefaultDB.initialize();
export default DefaultDB;