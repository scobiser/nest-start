import { ApiProperty } from "@nestjs/swagger";

export class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: Express.Multer.File;
}

export class FileUploadDtos {
  @ApiProperty({ type: 'string', format: 'binary' })
  avatar?: Express.Multer.File[];
  @ApiProperty({ type: 'string', format: 'binary' })
  background?: Express.Multer.File[] 
}
