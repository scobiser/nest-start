import { ApiProperty } from "@nestjs/swagger";
/**
 * ApiProperty 会体现在 Schema  和 example value
 */
import { IsNotEmpty, Length, IsString, IsNumber } from 'class-validator';
export class Pet {
    @IsNotEmpty({ message: '名字名不能为空' })
    @IsString()
    @ApiProperty({ description: '名字' })
    name: string;
    @IsNotEmpty({ message: '年龄不能为空' })
    @IsNumber()
    @ApiProperty({ description: '年龄' })
    age: number;
}
