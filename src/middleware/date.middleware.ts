import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class DateMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    console.log('now :>> ', new Date().getTime());
    next();
  }
}
