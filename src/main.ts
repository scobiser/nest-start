import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { global_middleware } from './middleware/global.middleware';
import { getLocalIP } from './utils/http-util/IP';
import { ConsoleLogger } from '@nestjs/common';
import { SERVER_PORT } from './constants/server.bootstrap.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: true,
    rawBody: true
  });
  const logger = new ConsoleLogger('BootStrap', { logLevels: ['log'] });
  app.use(global_middleware);

  const options = new DocumentBuilder()
    .setTitle('Nest Starter')
    .setDescription('The Nest Starter API description')
    .setVersion('1.0')
    .addTag('Nest')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(SERVER_PORT, getLocalIP()).then((_) => {
    logger.log(`访问API地址: http://${getLocalIP()}:${SERVER_PORT}/api`);
    logger.log(`访问OPEN_API地址: http://${getLocalIP()}:${SERVER_PORT}/api-json`);
  });
}
bootstrap();
