import * as path from 'path';
import * as fs from 'fs';
import { getRUNNING_ENV } from './config.util';
import { registerAs } from '@nestjs/config';

export const ConfigData = registerAs('json', () => {
  const environment = getRUNNING_ENV();
  const yamlPath = path.join(process.cwd(), `./config.${environment}.json`);
  const jsonString = fs.readFileSync(yamlPath, 'utf8');
  const config = JSON.parse(jsonString);
  return config;
});