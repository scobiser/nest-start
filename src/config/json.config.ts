import * as path from 'path';
import * as fs from 'fs';
import { getRUNNING_ENV } from './config.util';

// 读取项目配置
export const getJsonConfig = () => {
  const environment = getRUNNING_ENV();
  const yamlPath = path.join(process.cwd(), `./config.${environment}.json`);
  const jsonString = fs.readFileSync(yamlPath, 'utf8');
  const config = JSON.parse(jsonString);
  return config;
};