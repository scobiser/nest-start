import { registerAs } from "@nestjs/config";

export function DataBase() {
    return {
        port: parseInt(process.env.PORT, 10) || 3000,
        database: {
            host: process.env.DB_HOST,
            port: parseInt(process.env.DATABASE_PORT, 10) || 5432
        }
    }
};

export const RegDataBase = registerAs('regdatabase', () => ({
    host: process.env.DB_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 1111
}));