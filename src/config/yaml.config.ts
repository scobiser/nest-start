import { parse } from 'yaml';
import * as path from 'path';
import * as fs from 'fs';
import { getRUNNING_ENV } from './config.util';

// 读取项目配置
export const getYamlConfig = () => {
  const environment = getRUNNING_ENV();
  // console.log(environment, '当前运行的环境');
  const yamlPath = path.join(process.cwd(), `./application.${environment}.yaml`);
  // console.log('yamlPath :>> ', yamlPath);
  const file = fs.readFileSync(yamlPath, 'utf8');
  const config = parse(file);
  return config;
};