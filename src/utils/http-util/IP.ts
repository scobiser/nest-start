import http from 'http';
export function getLocalIP() {
    const os = require('os');
    const ifaces = os.networkInterfaces();
    let locatIp = '';
    let hasFound = false;
    outer:
    for (let dev in ifaces) {
        if (hasFound) break;
        if (dev === '本地连接' || dev === '以太网' || dev === 'WLAN') {
            for (let j = 0; j < ifaces[dev].length; j++) {
                if (ifaces[dev][j].family === 'IPv4') {
                    locatIp = ifaces[dev][j].address;
                    break outer;
                }
            }
        }
    }
    return locatIp;
}

/**
 * 获取公网IP
 * @param {Function} fn 异步获取结果后的回调函数
 */
export function getPublicIP(fn) {
    http.get('http://ip.taobao.com/service/getIpInfo.php?ip=myip', res => {
        typeof fn === 'function' && fn(res);
        console.info(res);
    });
}

export default {
    getLocalIP,
    getPublicIP
};

