import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { People } from './People.entity';

@Entity({ name: "animal" })
export class Animal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  age: number;

  @ManyToOne(() => People, (people) => people.animals)
  @JoinColumn({ name: "pid" })
  people: People;
}