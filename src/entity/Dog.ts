import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { People } from './People.entity';

@Entity({ name: "Dog" })
export class Dog {
  @PrimaryGeneratedColumn({type:'int'})
  id: number;

  @Column({type:'varchar'})
  name: string;
  

  @Column({type:'int'})
  age: number;

  @ManyToOne(() => People, (people) => people.animals)
  @JoinColumn({ name: "pid" })
  people: People;
}