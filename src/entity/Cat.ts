import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { People } from './People.entity';

@Entity({ name: "Dog" })
export class Dog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  
  @Column()
  sex: string;
  @Column()
  sss: string;

  @Column()
  age: number;

  @ManyToOne(() => People, (people) => people.animals)
  @JoinColumn({ name: "pid" })
  people: People;
}