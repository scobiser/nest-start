import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Animal } from './Animal.entity';
import { People } from './People.entity';

@Entity({ name: 'role' })
export class Role {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    role: string;

    // @ManyToMany((type) => People, (people) => people.roles, {
    //     cascade: true,
    // })
    // @JoinTable()
    // peoples: People[]
}