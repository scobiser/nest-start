import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Animal } from './Animal.entity';
import { Role } from './role.entity';

@Entity({ name: 'people' })
export class People {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  username: string;
  @Column()
  age: number;
  @Column()
  password: string;

  @OneToMany(() => Animal, (animal) => animal.people)
  animals: Animal[]

  @ManyToMany((type) => Role)
  @JoinTable()
  roles: Role[]
}