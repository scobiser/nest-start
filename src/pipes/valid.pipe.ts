import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ValidPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    return checkFalse(value);
  }
}

function checkFalse(val: any) {
  console.log('val :>> ', val);
  if (checkType(val).includes('Null') || checkType(val).includes('Undefined') ) {
    throw new BadRequestException('参数为空');
  }
  return val;
}

function checkType(val: any) {
  return Object.prototype.toString.call(val);
}