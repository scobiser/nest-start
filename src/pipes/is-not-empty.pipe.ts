import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  BadRequestException,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
@Injectable()
export class IsNotEmptyPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype } = metadata;
    //生成对应类型的对象
    const object = plainToInstance(metatype, value);
    //利用dto中规则对数据进行验证
    const error = await validate(object);

    if (error.length > 0) {
      throw new BadRequestException(error);
    }
    return value;
  }
}
