## Document

> https://docs.nestjs.cn

## 环境

* node 

> 请确保在您的操作系统上安装了 Node.js (>= 10.13.0，v13 除外).

* cli  

> npm i -g @nestjs/cli

##  Controller不需要导出 : Controller 是一个 请求处理器 ，对应的模块被引入时就生效了

## Service可以导出，可以作为Provider

## API注解

> @ApiTags   API TAG

> @ApiQuery  查询参数 

> @ApiParam  路由参数 
