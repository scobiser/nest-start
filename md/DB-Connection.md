## TypeOrmOption

### MySQL 连接
```typescript
export const  DefaultDBOption: TypeOrmModuleOptions = {
  type: 'mysql',
  host: dbConfig.db.mysql.url,
  port: dbConfig.db.mysql.port,
  username: dbConfig.db.mysql.user,
  password: dbConfig.db.mysql.password,
  database: dbConfig.db.mysql.database,
  entities: [People, User, Animal, Re,Dog,Role],
  autoLoadEntities: true,
  synchronize: false
};

export const DefaultDB = TypeOrmModule.forRoot(DefaultDBOption);
```
