## Config

```typescript
    ConfigModule.forRoot({
      envFilePath,
      isGlobal: true,
      load: [DataBase, RegDataBase, getYamlConfig, YamlData,getJsonConfig,ConfigData]
    }),
```

## 1 .env 作为配置文件

> 在根目录创建.env文件

```typescript
export function DataBase() {
    return {
        port: parseInt(process.env.PORT, 10) || 3000,
        database: {
            host: process.env.DB_HOST,
            port: parseInt(process.env.DATABASE_PORT, 10) || 5432
        }
    }
};

export const RegDataBase = registerAs('regdatabase', () => ({
    host: process.env.DB_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 1111
}));
```

## 2. yaml 作为配置文件

```typescript
export const getYamlConfig = () => {
  const environment = getRUNNING_ENV();
  // console.log(environment, '当前运行的环境');
  const yamlPath = path.join(process.cwd(), `./application.${environment}.yaml`);
  // console.log('yamlPath :>> ', yamlPath);
  const file = fs.readFileSync(yamlPath, 'utf8');
  const config = parse(file);
  return config;
};
```

## 3.json 作为配置文件

```typescript
// 读取项目配置
export const getJsonConfig = () => {
  const environment = getRUNNING_ENV();
  const yamlPath = path.join(process.cwd(), `./config.${environment}.json`);
  const jsonString = fs.readFileSync(yamlPath, 'utf8');
  const config = JSON.parse(jsonString);
  return config;
};
```
