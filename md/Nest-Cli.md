## CLI 配置

```typescript
nest-cli.json
{
  "$schema": "https://json.schemastore.org/nest-cli",
  "collection": "@nestjs/schematics",
  "sourceRoot": "src",
  "compilerOptions": {
    "assets": [
      "assets/**/*",
      "./generator/**/*"
    ],
    "watchAssets": true,
    "tsConfigPath": "tsconfig.build.json"
  },
  "exclude": [
    "./generator/**/*"
  ]
}
```

###  nest-cli.json#assets 

> 静态目录，直接复制到dist

### exclude

> 额外的build配置

> 排除generator目录

```typescript
tsconfig.build.json
{
  "extends": "./tsconfig.json",
  "exclude": ["node_modules", "test", "dist", "**/*spec.ts","generator"]
}
```
